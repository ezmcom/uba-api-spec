### Scoring REST API ###

#### UBA Score ####
This API serves for the purpose of evaluate UBA data and get score.

* REST URL
```
/uba/v1/apiKey/{apiKey}/account/{accountName}/profile/{profileName}/scoreAndUpdate
```
```
/uba/v1/apiKey/{apiKey}/accounts/{accountName}/profiles/{profileName}/scoreAndUpdate
```

* Path Parameter

    Name  | Type | Mandatory | Max Length | Description |
    ----- | -----| ----------|------------|-------------|
    apiKey | Alphanumeric | Yes | N/A | UBA Application API Key |
    accountName | Alphanumeric and these characters: ```@``` ```_``` ```-``` ```.``` | Yes | 100 | UBA Customer Account Name. This is most likely the customer user identifier. |
    profileName | Alphanumeric and these characters: ```@``` ```_``` ```-``` ```.``` | Yes | 100 | UBA Customer Profile Name. Combination of Account Name and Profile Name will uniquely identify UBA Profile under an UBA Applicaiton. |

* Query Parameters

    Name  | Type | Mandatory | Max Length | Description |
    ----- | -----| ----------|------------|-------------|
    operation | N/A | No | N/A | UBA operation. This will indicate if the UBA scoring API call is for readonly-scoring or update-scoring mechanism. |

    The parameter ```operation``` may accept these values (case-insensitive):
    
    Value | Description |
    ----- | ------------|
    SCORE | Will treat the request as readonly, unless the score is greater than the threshold. This is done so that the caller will not need to call UPDATE. If the UBA account profile is still under training, it is expecting the caller to call the REST API again with the operation ```UPDATE``` to commit the training data. |
    UPDATE | UPDATE will treat the request as read write, updating the training data if the score is within range of the threshold | 
    SCOREANDUPDATE | Behave just like ```UPDATE```. This is a default value if the query parameter ```operation``` is either not found or not matching with one of the accepting values. |

* HTTP Request
    * HTTP Request Method
        * PUT
        * POST

    * HTTP Request Header: 
        * ```Content-Type``` : ```application/json```

    * HTTP Request Body: 

```
#!json

{
  "v":1,
  "ts":1490766750824,
  "fn":0,
  "sid":"9qy2tysnb1hwjw34n5r5411s5og96e65",
  "d":[[0,1490766748080,16],[0,1490766748184,78],[1,1490766748240,78],[1,1490766748256,16],[0,1490766748376,48],[1,1490766748448,48],[0,1490766748456,86],[0,1490766748536,69],[1,1490766748600,86],[1,1490766748648,69],[0,1490766748664,76],[1,1490766748728,76],[0,1490766748808,76],[1,1490766748864,76],[0,1490766748952,190],[1,1490766749032,190],[0,1490766749192,56],[1,1490766749264,56],[0,1490766749368,73],[1,1490766749424,73]],
  "h":"a85460c2f7b94bcf3cea39ba292337bcec4f5cbda640b2ce54b6f731a6150f30",
  "ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/601.7.8 (KHTML, like Gecko) Version/9.1.3 Safari/537.86.7"
}
```

* HTTP Response
    * HTTP Response Status Code: ```200```
    * HTTP Response Header: ```application/json;charset=UTF-8```
    * HTTP Response Body (Under Training and Training Completed)

```
#!json

{
  "score": "0.0",
  "training": "0",
  "attempt": "1",
  "transactionId": "7a13641f-61ea-4e32-b6f9-943a7473d921",
  "timestamp": 1490766639301,
  "message": "Success",
  "date": "2017-03-29 13:50:39",
  "returnCode": 0,
  "traceId": "20170329135039T7QC7w"
}
```

```
#!json

{
  "score": "0.9659281984802177",
  "threshold": "0.8665688974476878",
  "training": "1",
  "transactionId": "809a114b-973c-41b1-a757-fee2f3960ea2",
  "timestamp": 1490766756794,
  "message": "Success",
  "date": "2017-03-29 13:52:36",
  "returnCode": 0,
  "traceId": "20170329135236vF6yIw"
}
```

### Deferred Scoring REST API ###

#### Put UBA Data ####
This API serves for the purpose of storing the UBA data for deferred update. 

* REST URL
```
/uba/v1/ubastore/put
```

* HTTP Request
    * HTTP Request Method
        * PUT
        * POST

    * HTTP Request Header: 
        * ```Content-Type``` : ```application/json```

    * HTTP Request Body: 

```
#!json

{
  "v":1,
  "ts":1490766750824,
  "fn":0,
  "sid":"9qy2tysnb1hwjw34n5r5411s5og96e65",
  "d":[[0,1490766748080,16],[0,1490766748184,78],[1,1490766748240,78],[1,1490766748256,16],[0,1490766748376,48],[1,1490766748448,48],[0,1490766748456,86],[0,1490766748536,69],[1,1490766748600,86],[1,1490766748648,69],[0,1490766748664,76],[1,1490766748728,76],[0,1490766748808,76],[1,1490766748864,76],[0,1490766748952,190],[1,1490766749032,190],[0,1490766749192,56],[1,1490766749264,56],[0,1490766749368,73],[1,1490766749424,73]],
  "h":"a85460c2f7b94bcf3cea39ba292337bcec4f5cbda640b2ce54b6f731a6150f30",
  "ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/601.7.8 (KHTML, like Gecko) Version/9.1.3 Safari/537.86.7"
}
```

* HTTP Response
    * HTTP Response Status Code: ```200```
    * HTTP Response Header: ```application/json;charset=UTF-8```
    * HTTP Response Body:

```
#!json

{
  "key": "fe3caad1-7ef5-4cc2-a319-834f47144e83",
  "transactionId": "5ef91047-0708-4151-924d-187b0f36113e",
  "timestamp": 1490769031229,
  "message": "Success",
  "date": "2017-03-29 14:30:31",
  "returnCode": 0,
  "traceId": "2017032914303172BQcx"
}
```

#### UBA Score Deferred Update ####
This API serves for the purpose of evaluate and get score for deferred update UBA data.

* REST URL
```
/uba/v1/ubastore/apiKey/{apiKey}/account/{accountName}/profile/{profileName}/key/{key}/deferredScoreAndUpdate
```

```
/uba/v1/ubastore/apiKey/{apiKey}/accounts/{accountName}/profiles/{profileName}/key/{key}/deferredScoreAndUpdate
```

* Path Parameter

    Name  | Type | Mandatory | Max Length | Description |
    ----- | -----| ----------|------------|-------------|
    apiKey | Alphanumeric | Yes | N/A | UBA Application API Key |
    accountName | Alphanumeric and these characters: ```@``` ```_``` ```-``` ```.``` | Yes | 100 | UBA Customer Account Name. This is most likely the customer user identifier. |
    profileName | Alphanumeric | Yes | 16 | UBA Customer Profile Name. Combination of Account Name and Profile Name will uniquely identify UBA Profile under an UBA Applicaiton. |
    key | Alphanumeric | Yes | N/A | UBA deferred update stored data key. This value is obtained from the previous REST API ```/uba/v1/ubastore/put``` |

* Query Parameters

    Name  | Type | Mandatory | Max Length | Description |
    ----- | -----| ----------|------------|-------------|
    operation | N/A | No | N/A | UBA operation. This will indicate if the UBA scoring API call is for readonly-scoring or update-scoring mechanism. |

    The parameter ```operation``` may accept these values (case-insensitive):
    
    Value | Description |
    ----- | ------------|
    SCORE | Will treat the request as readonly, unless the score is greater than the threshold. This is done so that the caller will not need to call UPDATE. If the UBA account profile is still under training, it is expecting the caller to call the REST API again with the operation ```UPDATE``` to commit the training data. |
    UPDATE | UPDATE will treat the request as read write, updating the training data if the score is within range of the threshold | 
    SCOREANDUPDATE | Behave just like ```UPDATE```. This is a default value if the query parameter ```operation``` is either not found or not matching with one of the accepting values. |

* HTTP Request Method
    * PUT
    * POST

* HTTP Response
    * HTTP Response Status Code: ```200```
    * HTTP Response Header: ```application/json;charset=UTF-8```
    * HTTP Response Body (Under Training and Training Completed)

```
#!json

{
  "score": "0.0",
  "training": "0",
  "attempt": "1",
  "transactionId": "7a13641f-61ea-4e32-b6f9-943a7473d921",
  "timestamp": 1490766639301,
  "message": "Success",
  "date": "2017-03-29 13:50:39",
  "returnCode": 0,
  "traceId": "20170329135039T7QC7w"
}
```

```
#!json

{
  "score": "0.9659281984802177",
  "threshold": "0.8665688974476878",
  "training": "1",
  "transactionId": "809a114b-973c-41b1-a757-fee2f3960ea2",
  "timestamp": 1490766756794,
  "message": "Success",
  "date": "2017-03-29 13:52:36",
  "returnCode": 0,
  "traceId": "20170329135236vF6yIw"
}
```

### Reset UBA Account Profile ###

#### Reset UBA Account Profile Scoring Data ####
This API serves for the purpose of resetting the existing UBA account profile, which mean the customer will restart the training.

* REST URL
````
/uba/v1/apiKey/{apiKey}/account/{accountName}/profile/{profileName}/reset
````
````
/uba/v1/apiKey/{apiKey}/accounts/{accountName}/profiles/{profileName}/reset
````

* Path Parameter

    Name  | Type | Mandatory | Max Length | Description |
    ----- | -----| ----------|------------|-------------|
    apiKey | Alphanumeric | Yes | N/A | UBA Application API Key |
    accountName | Alphanumeric and these characters: ```@``` ```_``` ```-``` ```.``` | Yes | 100 | UBA Customer Account Name. This is most likely the customer user identifier. |
    profileName | Alphanumeric | Yes | 16 | UBA Customer Profile Name. Combination of Account Name and Profile Name will uniquely identify UBA Profile under an UBA Applicaiton. |

* HTTP Request Method
    * PUT
    * POST
    * GET

* HTTP Response
    * HTTP Response Status Code: ```200```
    * HTTP Response Header: ```application/json;charset=UTF-8```
    * HTTP Response Body:

```
#!json

{
  "transactionId": "8a8903fb-dba0-4b0f-bd21-68f40f4bc1d7",
  "timestamp": 1490768144071,
  "message": "Success",
  "date": "2017-03-29 14:15:44",
  "returnCode": 0,
  "traceId": "20170329141543UDPgsR"
}
```

* CURL Request Example (example below reset scoring data on an user profile with an Account Name "testacc" and Profile Name "password" under application with API Key "eee2a952-87d5-445c-9540-84c10e3deabf")
```
curl -k https://localhost:9843/uba-webservice/uba/v1/apiKey/eee2a952-87d5-445c-9540-84c10e3deabf/account/testacc/profile/password/reset
```

